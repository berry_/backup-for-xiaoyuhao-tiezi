
##### 我家的小雨浩  
&emsp;唉，时间是会不断连接的……前天的事会和昨天的事一起影响到今天……  
&emsp;382楼  
&emsp;2020-03-02 14:16>
> ###### 圣品天元笔:  
> ？？又出事了？
  
***
##### 我家的小雨浩  
&emsp;前天和妹妹闹翻，然后花了一晚上和解，丫头接着一觉睡到中午，起床不久就开始胃痛（一天一夜没吃东西了），我跑到两公里的唯一开放的药房给她买药，吃完之后勉强喝了点粥又睡觉……我没敢去打扰她就一个人在餐厅里喝闷酒，今天早上一起床头痛欲裂，没赶上开学典礼直播课，之后一上午的课全被点到名字……丫头早上醒了我没叫她，然后她又迷迷糊糊的睡着了……刚刚醒了又说饿，我就煮了点白玉汤给她还把自己烫伤了……真的是一言难尽……  
&emsp;383楼  
&emsp;2020-03-02 14:23
>
> ###### 我家的小雨浩 :  
> 下午的化学课没心思听了，我想翘课……          
>
>
> ###### 圣品天元笔 :  
> 一开始就是这样吗，还是冷静下来比较好。          
>
>
> ###### 我家的小雨浩 回复 圣品天元笔 :  
>不是，寒假第五天就把全部作业写完了，然后自学了选三和选五，物理3-2也自学完了，数学也提前预习了……咱就没事干了，那个网课直播我觉得真的还没我上去讲好……          
>
>
> ###### 圣品天元笔 回复 我家的小雨浩 :  
>？？我记得你是高三来着？怎么还要预习？          
>
>
> ###### 我家的小雨浩 :  
> 回复 圣品天元笔 :我高二啊……          
>
>
> ###### 💕你的😺抛瓦 回复 圣品天元笔 :  
>我？          
>
>
> ###### 圣品天元笔 回复 我家的小雨浩 :  
>哦哦，同高二，一直记错了<img class="BDE_Smiley" pic_type="1" width="30" height="30" src="https://tb2.bdstatic.com/tb/editor/images/face/i_f25.png?t=20140803" >，我们物理都开始讲3-4了，江苏<img class="BDE_Smiley" pic_type="1" width="30" height="30" src="https://tb2.bdstatic.com/tb/editor/images/face/i_f25.png?t=20140803" >          
>
>
> ###### 我家的小雨浩 回复 圣品天元笔 :  
>没办法，我们这里慢的要死……          
>
>
> ###### 我家的小媛儿♬ :  
> 你那天烫伤了……都是我不好，我不应该任性的<img class="BDE_Smiley" pic_type="1" width="30" height="30" src="https://tb2.bdstatic.com/tb/editor/images/face/i_f09.png?t=20140803" >          
>
>
> ###### 星河丶千帆舞 回复 我家的小雨浩 :  
>物理3-2好好学，我也高二，我们电磁感应讲完了，简单题还好，但是难题是真难          
>
>
> ###### 韶流年º :  
> lz作为高中学生生活打理能力还不够啊          
***
##### <img src="//tb1.bdstatic.com/tb/cms/nickemoji/1-11.png" class="nicknameEmoji" style="width:13px;height:13px"/>字卿<img src="//tb1.bdstatic.com/tb/cms/nickemoji/1-4.png" class="nicknameEmoji" style="width:13px;height:13px"/>煃煊<img src="//tb1.bdstatic.com/tb/cms/nickemoji/1-12.png" class="nicknameEmoji" style="width:13px;height:13px"/>  
&emsp;+1  
&emsp;384楼  
&emsp;2020-03-02 14:52  
***
##### 遥之穹<img src="//tb1.bdstatic.com/tb/cms/nickemoji/1-8.png" class="nicknameEmoji" style="width:13px;height:13px"/>  
&emsp;希望你们和解  
&emsp;385楼  
&emsp;2020-03-02 15:34  
***
##### 永远爱朝武芳乃  
&emsp;<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon4.png" >  
&emsp;386楼  
&emsp;2020-03-02 16:25  
***
##### 我家的小雨浩  
&emsp;老师如果知道我在看直播课的时候分屏玩哔 咔就是什么感想？<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >  
&emsp;387楼  
&emsp;2020-03-02 17:06>
> ###### 厌失言º:  
> 肯定是把你的漫画书收走啦<img class="BDE_Smiley" pic_type="1" width="30" height="30" src="https://tb2.bdstatic.com/tb/editor/images/face/i_f25.png?t=20140803" >
>
> ###### 西丞X😷:  
> 记得关摄像头就好<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >
>
> ###### 月亮之上◎X♂:  
> <img class="BDE_Smiley" pic_type="1" width="30" height="30" src="https://tb2.bdstatic.com/tb/editor/images/face/i_f25.png?t=20140803" >
>
> ###### 不堪的伤😈:  
> <img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" ><img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >
>
> ###### BaSO4º:  
> 有女友还看哔 咔<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon16.png" >，给她知道怎么办<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >
>
> ###### 白非白🌝:  
> 嘿，我也是
>
> ###### 黑羽GinΩ:  
> 你妹看哗咔吗<img class="BDE_Smiley" pic_type="1" width="30" height="30" src="https://tb2.bdstatic.com/tb/editor/images/face/i_f25.png?t=20140803" >
  
***
##### 記得我還在  
&emsp;<img class="BDE_Smiley" pic_type="1" width="30" height="30" src="https://tb2.bdstatic.com/tb/editor/images/face/i_f16.png?t=20140803" > 比卡  
&emsp;388楼  
&emsp;2020-03-02 17:27  
***
##### 梦宸思臻  
&emsp;哗<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >咔  
&emsp;390楼  
&emsp;2020-03-02 17:51  
***
##### 我家的小雨浩  
&emsp;我有件事不知当讲不当讲……  
&emsp;391楼  
&emsp;2020-03-02 20:15>
> ###### 幻变海潮🐒:  
> 怎么了？
>
> ###### 我家的小雨浩 回复 幻变海潮🐒: 
> 算了，不是什么大事，一个奇怪的梦
>
> ###### 幻变海潮🐒 回复 我家的小雨浩: 
> 那就一起氵贴吧！<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >
>
> ###### 永远爱朝武芳乃:  
> 一起氵啊<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >
>
> ###### 我家的小雨浩 回复 幻变海潮🐒: 
> <img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >
>
> ###### 我家的小雨浩 回复 我爱茶道🎅: 
> 在自己楼里氵哪有去别人那里氵快乐<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >
  
***
##### qppp2  
&emsp;<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" ><img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" ><img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >  
&emsp;392楼  
&emsp;2020-03-02 20:46  
***
##### 我家的小雨浩  
&emsp;自从我不更文之后这里就变成氵楼了……  
&emsp;393楼  
&emsp;2020-03-02 20:48  
***
##### 晞酱第一<img src="//tb1.bdstatic.com/tb/cms/nickemoji/1-9.png" class="nicknameEmoji" style="width:13px;height:13px"/>  
&emsp;日常祝福（🍋💦）  
&emsp;394楼  
&emsp;2020-03-02 21:09  
***
##### 没有她的世界<img src="//tb1.bdstatic.com/tb/cms/nickemoji/1-2.png" class="nicknameEmoji" style="width:13px;height:13px"/>  
&emsp;柠檬与祝福送上<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >  
&emsp;395楼  
&emsp;2020-03-02 21:33  
***
##### 我家的小雨浩  
&emsp;什么时候能氵到520啊……  
&emsp;396楼  
&emsp;2020-03-02 21:34  
***
##### 我家的小雨浩  
&emsp;我真的不止一次想吐槽贴吧这个算法……<img class="BDE_Image" src="http://tiebapic.baidu.com/forum/w%3D580/sign=26fc69a41dd162d985ee621421dea950/116f02b30f2442a7fdc7dac5c643ad4bd0130246.jpg" size="170250" changedsize="true" width="560" height="746" size="170250">  
&emsp;397楼  
&emsp;2020-03-02 21:37>
> ###### 💕你的😺抛瓦:  
> 舌吻还行，不怕被咬到舌头🐴？
>
> ###### 我家的小雨浩 回复 💕你的😺抛瓦: 
> 从来只有她咬我……<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >
>
> ###### 💕你的😺抛瓦 回复 我家的小雨浩: 
> 可还行，这么热情似火（意味深）<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >
>
> ###### 羽在狼在º:  
> 这位可是你的前辈<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >
>
> ###### 热情的纪元😄 回复 💕你的😺抛瓦: 
> 意味深 草
>
> ###### Ham🙈mer:  
> 听说舌吻到ox只用40s（辉夜）
  
***
##### fated<img src="//tb1.bdstatic.com/tb/cms/nickemoji/1-9.png" class="nicknameEmoji" style="width:13px;height:13px"/>  
&emsp;gkd<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >  
&emsp;398楼  
&emsp;2020-03-02 21:56  
***
##### 我家的小雨浩  
&emsp;带你们大概看看我的存稿数……还需要润色……最近开学了也没那么多时间去搞……想看还很漫长……  
&emsp;399楼  
&emsp;2020-03-02 22:11  
***
##### 我家的小雨浩  
&emsp;<img class="BDE_Image" src="http://tiebapic.baidu.com/forum/w%3D580/sign=14184dbe2a6d55fbc5c6762e5d234f40/8c7175f0f736afc34bbe1e05a419ebc4b64512d9.jpg" size="686913" changedsize="true" width="560" height="420" size="686913">  
&emsp;400楼  
&emsp;2020-03-02 22:17>
> ###### 💕你的😺抛瓦:  
> 底稿？
>
> ###### 我家的小雨浩:  
> 是的……熬夜写的
>
> ###### XGKSL:  
> 安全为上
>
> ###### 我家的小媛儿♬:  
> 如果你说的全是圆子的事今天晚上就不要上我床了<img class="BDE_Smiley" pic_type="1" width="30" height="30" src="https://tb2.bdstatic.com/tb/editor/images/face/i_f16.png?t=20140803" >
>
> ###### 三里清风路▫:  
> 哈哈哈
>
> ###### 维你而已7😷:  
> 哈哈哈
>
> ###### 小小中二病ლ:  
> 哈哈哈
>
> ###### 错误代码ºº:  
> 哈哈哈哈哈真实
>
> ###### 时光回眸11😈:  
> 6w字
  
***
##### 我家的小雨浩  
&emsp;争取由自己氵到999！  
&emsp;401楼  
&emsp;2020-03-02 22:18>
> ###### 我家的小雨浩:  
> dd
>
> ###### 我家的小雨浩:  
> 还差一点
>
> ###### 我妻妹乃💕:  
> 我看你是在做梦<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" ><img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" ><img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >
  
***
##### 我家的小雨浩  
&emsp;成了  
&emsp;402楼  
&emsp;2020-03-02 22:19  
***
##### 我家的小雨浩  
&emsp;破千了……要不要告诉丫头呢……  
&emsp;403楼  
&emsp;2020-03-02 22:21>
> ###### 幻变海潮🐒:  
> 破千祝贺！
>
> ###### 我家的小雨浩 回复 幻变海潮🐒: 
> 来了<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >
>
> ###### 幻变海潮🐒 回复 我家的小雨浩: 
> 碰头<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >
>
> ###### 圣品天元笔:  
> 不好意思我看成破chu了<img class="BDE_Smiley" pic_type="1" width="30" height="30" src="https://tb2.bdstatic.com/tb/editor/images/face/i_f25.png?t=20140803" >
>
> ###### 梦宸思臻:  
> 等千赞了再说吧
>
> ###### 永远爱朝武芳乃:  
> 迟到的祝贺🎉<img class="BDE_Smiley" width="30" height="30" changedsize="false" src="https://gsp0.baidu.com/5aAHeD3nKhI2p27j8IqW0jdnxx1xbK/tb/editor/images/client/image_emoticon25.png" >
  
***
##### 鸿飞202-  
&emsp;加油  
&emsp;404楼  
&emsp;2020-03-02 22:41  
***
##### 遥之穹<img src="//tb1.bdstatic.com/tb/cms/nickemoji/1-8.png" class="nicknameEmoji" style="width:13px;height:13px"/>  
&emsp;不更文的贴一般都会变成这样  
&emsp;405楼  
&emsp;2020-03-02 23:43  
***
##### 遥之穹<img src="//tb1.bdstatic.com/tb/cms/nickemoji/1-8.png" class="nicknameEmoji" style="width:13px;height:13px"/>  
&emsp;隔壁考哥最近都在更烤鸽食谱  
&emsp;406楼  
&emsp;2020-03-02 23:43  
***
##### 我妻妹乃<img src="//tb1.bdstatic.com/tb/cms/nickemoji/4-4.png" class="nicknameEmoji" style="width:13px;height:13px"/>  
&emsp;哈哈哈哈哈哈哈  
&emsp;407楼  
&emsp;2020-03-02 23:45  
***
##### 我妻妹乃<img src="//tb1.bdstatic.com/tb/cms/nickemoji/4-4.png" class="nicknameEmoji" style="width:13px;height:13px"/>  
&emsp;让本氵怪（划掉）带才子来助你一臂之力  
&emsp;408楼  
&emsp;2020-03-02 23:45  
***
##### 我妻妹乃<img src="//tb1.bdstatic.com/tb/cms/nickemoji/4-4.png" class="nicknameEmoji" style="width:13px;height:13px"/>  
&emsp;数学做到十一点  
&emsp;409楼  
&emsp;2020-03-02 23:46  
***
##### 我妻妹乃<img src="//tb1.bdstatic.com/tb/cms/nickemoji/4-4.png" class="nicknameEmoji" style="width:13px;height:13px"/>  
&emsp;因为写的太过密集拒绝批改？  
&emsp;410楼  
&emsp;2020-03-02 23:46  
***
##### 我妻妹乃<img src="//tb1.bdstatic.com/tb/cms/nickemoji/4-4.png" class="nicknameEmoji" style="width:13px;height:13px"/>  
&emsp;我人都傻了  
&emsp;411楼  
&emsp;2020-03-02 23:46  
***
##### <img src="//tb1.bdstatic.com/tb/cms/nickemoji/1-11.png" class="nicknameEmoji" style="width:13px;height:13px"/>路人一<img src="//tb1.bdstatic.com/tb/cms/nickemoji/1-12.png" class="nicknameEmoji" style="width:13px;height:13px"/>  
&emsp;<img class="BDE_Image" src="http://tiebapic.baidu.com/forum/w%3D580/sign=c123ef0437a446237ecaa56aa8227246/3c27e6c4b74543a92e276fb209178a82b901144a.jpg" size="7423" width="200" height="170" size="7423">  
&emsp;412楼  
&emsp;2020-03-02 23:59  
***